package com.company;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.lang.management.ThreadInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws Exception {

        System.setProperty("webdriver.chrome.driver", "src/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("http://javarush.ru/login.html");
        WebElement element = driver.findElement(By.xpath(".//*[@id='rightmenu1']/form[3]/div/button"));
        element.submit();

        element = driver.findElement(By.name("email"));
        element.sendKeys("Ivan4784@mail.ru");

        element = driver.findElement(By.name("pass"));
        element.sendKeys("miptdrec");
        element.submit();

        driver.get("http://javarush.ru/levels/tasks.html?v=7");
        TimeUnit.SECONDS.sleep(3);

        //element = driver.findElement(By.id("task_button_11"));
        solveAllProblem(driver);

        System.out.println("Test completed!");
    }

    // This method writes a string to the system clipboard.
    // otherwise it returns null.
    private static void sendBuffer(String str) {
        StringSelection ss = new StringSelection(str);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
    }

    private static void scroll(WebDriver driver, int x, int y) {
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(" + x + ","
                + y + ");");
    }

    private static void scrollWithOffset(WebElement webElement, WebDriver driver, int x, int y) {

        String code = "window.scroll(" + (webElement.getLocation().x + x) + ","
                + (webElement.getLocation().y + y) + ");";

        ((JavascriptExecutor) driver).executeScript(code, webElement, x, y);

    }

    private static void testScroll(WebDriver driver) {
        Actions builder = new Actions(driver);
        builder.sendKeys(Keys.chord(Keys.ARROW_DOWN)).perform();
    }

    private static void solveProblem(WebDriver driver, WebElement element) throws Exception {
        element.click();
        TimeUnit.SECONDS.sleep(1);

        element = driver.findElement(By.id("pCopyEditorOriginalCode_JavaEditor"));

        element.click();
        List<String> endList = new ArrayList<String>();

        List<WebElement> lineGroups = element.findElements(By.className("ace_line_group"));
        for (WebElement group : lineGroups) {
            List<WebElement> lines = group.findElements(By.className("ace_line"));
            for (WebElement line : lines) {
                String tmp = line.getText() + "\n";
                endList.add(tmp);
            }
        }
        endList.remove(endList.size() - 1);

        for (int i = 0; i < 10; i++) {
            testScroll(driver);
        }

        List<WebElement> lines = null;
        WebElement lastline = null;
        String pastlast = "";
        for (int i = 0; i < 100; i++) {
            lines = driver.findElements(By.className("ace_line"));
            if (!pastlast.equals(lines.get(lines.size() - 3).getText() + "\n")) {
                lastline = lines.get(lines.size() - 3);
            } else {
                lastline = lines.get(lines.size() - 2);
            }
            //---bag with big solve
            String tmp = lastline.getText() + "\n";
            if (!endList.get(endList.size() - 1).equals(tmp)) {
                endList.add(tmp);
            }
            pastlast = lines.get(lines.size() - 3).getText() + "\n";

            testScroll(driver);
            //------------
        }
        element = driver.findElement(By.id("pCopyEditorUserCode"));
        element = element.findElement(By.id("pCopyEditorUserCode_JavaEditor"));
        element = element.findElement(By.className("ace_text-input"));

        for (String cur : endList) {
            sendBuffer(cur);
            element.sendKeys(Keys.CONTROL + "v");
            //System.out.print(cur);
        }
        Thread.sleep(1000);
        element = driver.findElement(By.className("top-message-panel-button2"));
        element.click();
        Thread.sleep(5000);

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated
                (By.xpath(".//*[@id='pCopyEditorCloseButtonPanel']/button")));
        driver.findElement(By.xpath(".//*[@id='pCopyEditorCloseButtonPanel']/button")).click();

        TimeUnit.SECONDS.sleep(1);
    }

    private static void solveAllProblem(WebDriver driver) throws Exception {
        int i = 28;
        while (true) {
            List<WebElement> problems = driver.findElements(By.id("task_button_" + i));
            if (problems.size() == 0) {
                return;
            }
            WebElement element = problems.get(problems.size() - 1);

            solveProblem(driver, element);
            i++;
        }
    }
}