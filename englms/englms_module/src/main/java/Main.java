import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.xml.bind.Element;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("https://www.cambridgelms.org/main/p/splash");
        System.out.println("Opened main window");

        WebElement element = driver.findElement(By.id("cas_iframe"));
        driver.switchTo().frame(element);

        element = driver.findElement(By.id("username"));
        element.sendKeys("sky731");

        element = driver.findElement(By.id("password"));
        element.sendKeys("gfhjkm4784");
        element.submit();

        driver.get("https://www.cambridgelms.org/main/p/en/" +
                "class/16883987/content_home/2476");
        element = driver.findElement(By.cssSelector("#course-management-form > div" +
                " > ul > li:nth-child(1) > a"));
        element.click();

        try {
            Thread.sleep(5000);
            System.out.println("Sleeped 5s");
        } catch (Exception ex) {
            System.out.println("CANT SLEEP");
        }


        List<WebElement> list = driver.findElements(By.cssSelector(".lesson-title.clearfix.student-enable"));
        System.out.println("Count elements = " + list.size());

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getText());
        }

        for (int i = 0; i < list.size(); i++) {
            list.get(i).click();
        }
    }
}
