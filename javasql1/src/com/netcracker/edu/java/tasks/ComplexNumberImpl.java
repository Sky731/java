package com.netcracker.edu.java.tasks;

import java.util.Arrays;

public class ComplexNumberImpl implements ComplexNumber {
    public ComplexNumberImpl() { }

    private double re = 0;
    private double im = 0;

    @Override
    public double getRe() {
        return re;
    }

    @Override
    public double getIm() {
        return im;
    }

    @Override
    public boolean isReal() {
        return im == 0;
    }

    @Override
    public void set(double re, double im) {
        this.re = re;
        this.im = im;
    }

    @Override
    public void set(String value) throws NumberFormatException {
        if (value.indexOf("+", 1) != -1 ||
                value.indexOf("-", 1) != -1) {
            int startIndex = value.indexOf("+", 1);
            startIndex = value.indexOf("-", 1) != -1 ?
                    value.indexOf("-", 1) : startIndex;
            String sub = value.substring(0, startIndex);
            re = Double.parseDouble(sub);
            int iIndex = value.indexOf("i", startIndex);
            if (startIndex + 1 == iIndex) {
                if(value.indexOf('-', 1) == startIndex) {
                    im = -1;
                } else {
                    im = 1;
                }
                return;
            }
            sub = value.substring(startIndex, iIndex);
            im = Double.parseDouble(sub);
        } else {
            int iIndex = value.indexOf('i');
            if (iIndex == 0) {
                im = 1;
                return;
            }
            if (iIndex != -1) {
                im = Double.parseDouble(value.substring(0, iIndex));
            } else {
                re = Double.parseDouble(value);
            }
        }
    }

    @Override
    public ComplexNumber copy() {
        ComplexNumber number = new ComplexNumberImpl();
        number.set(this.getRe(), this.getIm());
        return number;
    }

    @Override
    public ComplexNumber clone() throws CloneNotSupportedException {
        return (ComplexNumber) super.clone();
    }

    @Override
    public String toString() {
        if (re == 0 && im != 0) {
            return im + "i";
        } else if (im == 0 && re != 0) {
            return Double.toString(re);
        } else if (im != 0 && re != 0) {
            if (im > 0) {
                return Double.toString(re) + "+" + Double.toString(im) + "i";
            } else {
                return Double.toString(re) + Double.toString(im) + "i";
            }
        } else {
            return "0.0";
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ComplexNumber) {
            return getIm() == ((ComplexNumber)other).getIm() &&
                    getRe() == ((ComplexNumber)other).getRe();
        } else {
            return false;
        }

    }

    @Override
    public int compareTo(ComplexNumber other) {
        double lengthThis = getRe()*getRe() + getIm()*getIm();
        double length = other.getRe()*other.getRe() + other.getIm()*other.getIm();
        return (int) (lengthThis - length);
    }

    @Override
    public void sort(ComplexNumber[] array) {
        Arrays.sort(array);
    }

    @Override
    public ComplexNumber negate() {
        this.re *= -1;
        this.im *= -1;
        return this;
    }

    @Override
    public ComplexNumber add(ComplexNumber arg2) {
        this.re += arg2.getRe();
        this.im += arg2.getIm();
        return this;
    }

    @Override
    public ComplexNumber multiply(ComplexNumber arg2) {
        double reNew = re * arg2.getRe() - im * arg2.getIm();
        double imNew = im * arg2.getRe() + re * arg2.getIm();
        re = reNew;
        im = imNew;
        return this;
    }
}

