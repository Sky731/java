package ru.ncedu.java.tasks;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayVectorImpl implements ArrayVector {
    public ArrayVectorImpl() { }

    private double[] coords;

    @Override
    public void set(double... elements) {
        coords = elements;
    }

    @Override
    public double[] get() {
        return coords;
    }

    @Override
    public ArrayVector clone() {
        ArrayVector ar = new ArrayVectorImpl();
        ar.set(coords.clone());
        return ar;
    }

    @Override
    public int getSize() {
        return coords.length;
    }

    @Override
    public void set(int index, double value) {
        if (index >= 0 && index < coords.length) {
            coords[index] = value;
        } else if (index >= coords.length) {
            coords = Arrays.copyOf(coords, index + 1);
            coords[index] = value;
        } else {
            return;
        }
    }

    @Override
    public double get(int index) throws ArrayIndexOutOfBoundsException {
        return coords[index];
    }

    @Override
    public double getMax() {
        double max = coords[0];
        for (double elem : coords) {
            max = max > elem ? max : elem;
        }
        return max;
    }

    @Override
    public double getMin() {
        double min = coords[0];
        for (double elem : coords) {
            min = min < elem ? min : elem;
        }
        return min;
    }

    @Override
    public void sortAscending() {
        for (int i = 0; i < coords.length; i++) {
            for (int j = 0; j < coords.length - i - 1; j++) {
                if (coords[j] > coords[j + 1]) {
                    double tmp = coords[j];
                    coords[j] = coords[j + 1];
                    coords[j + 1] = tmp;
                }
            }
        }
    }

    @Override
    public void mult(double factor) {
        for (int i = 0; i < coords.length; i++) {
            coords[i] *= factor;
        }
    }

    @Override
    public ArrayVector sum(ArrayVector anotherVector) {
        if (anotherVector instanceof ArrayVectorImpl) {
            if (coords.length >= ((ArrayVectorImpl) anotherVector).coords.length) {
                for (int i = 0; i < ((ArrayVectorImpl) anotherVector).coords.length; i++) {
                    coords[i] += ((ArrayVectorImpl) anotherVector).coords[i];
                }
            } else {
                for (int i = 0; i < coords.length; i++) {
                    coords[i] += ((ArrayVectorImpl) anotherVector).coords[i];
                }
            }
        } else {
            return this;
        }
        return this;
    }

    @Override
    public double scalarMult(ArrayVector anotherVector) {
        if (anotherVector instanceof ArrayVectorImpl) {
            double sum = 0;
            if (coords.length >= ((ArrayVectorImpl) anotherVector).coords.length) {
                for (int i = 0; i < ((ArrayVectorImpl) anotherVector).coords.length; i++) {
                    sum += coords[i] * ((ArrayVectorImpl) anotherVector).coords[i];
                }
            } else {
                for (int i = 0; i < coords.length; i++) {
                    sum += coords[i] * ((ArrayVectorImpl) anotherVector).coords[i];
                }
            }
            return sum;
        } else {
            return 0;
        }
    }

    @Override
    public double getNorm() {
        return Math.sqrt(scalarMult(this));
    }
}
