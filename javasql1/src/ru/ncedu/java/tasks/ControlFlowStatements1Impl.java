package ru.ncedu.java.tasks;

public class ControlFlowStatements1Impl implements ControlFlowStatements1 {
    public ControlFlowStatements1Impl() {

    }

    @Override
    public String decodeWeekday(int weekday) {
        switch (weekday) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
        }
        return null;
    }

    @Override
    public float getFunctionValue(float x) {
        if (x > 0) {
            return (float) (2 * Math.sin((double) x));
        } else {
            return 6 - x;
        }
    }

    @Override
    public BankDeposit calculateBankDeposit(double P) {
        BankDeposit deposit = new BankDeposit();
        deposit.amount = 1000;
        while (deposit.amount < 5000) {
            deposit.amount *= ((P / 100) + 1);
            deposit.years++;
        }
        return deposit;
    }

    @Override
    public int getMinValue(int[][] array) {
        int min = 2147483647;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                min = min > array[i][j] ? array[i][j] : min;
            }
        }
        return min;
    }

    @Override
    public int[][] initArray() {
        int[][] array = new int[8][5];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 5; j++) {
                array[i][j] = i * j;
            }
        }
        return array;
    }

    public static void main(String[] args) {
        ControlFlowStatements1 statements1 = new ControlFlowStatements1Impl();
        BankDeposit deposit = statements1.calculateBankDeposit(15);
        System.out.printf("Ur cash " + deposit.years + " years later: $" + deposit.amount);
    }
}
