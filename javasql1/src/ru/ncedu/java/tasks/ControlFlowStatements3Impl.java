package ru.ncedu.java.tasks;

public class ControlFlowStatements3Impl implements ControlFlowStatements3 {
    public ControlFlowStatements3Impl() { }

    @Override
    public double getFunctionValue(double x) {
        if (x <= 0) {
            return -x;
        } else if (x > 0 && x < 2) {
            return x * x;
        } else {
            return 4;
        }
    }

    @Override
    public String decodeSeason(int monthNumber) {
        if (monthNumber == 1 || monthNumber == 2 || monthNumber == 12) {
            return "Winter";
        }
        if (monthNumber == 3 || monthNumber == 4 || monthNumber == 5) {
            return "Spring";
        }
        if (monthNumber == 6 || monthNumber == 7 || monthNumber == 8) {
            return "Summer";
        }
        if (monthNumber == 9 || monthNumber == 10 || monthNumber == 11) {
            return "Autumn";
        }
        return "Error";
    }
    @Override
    public long[][] initArray() {
        long[][] array = new long[8][5];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 5; j++) {
                array[i][j] = (long) Math.pow(Math.abs(i - j), 5);
            }
        }
        return array;
    }

    @Override
    public int getMaxProductIndex(long[][] array) {
        int index = 0;
        long maxSum = 0;
        long tmp = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                tmp *= array[i][j];
            }
            if (Math.abs(tmp) > maxSum) {
                maxSum = Math.abs(tmp);
                tmp = 1;
                index = i;
            }
        }
        return index;
    }

    @Override
    public float calculateLineSegment(float A, float B) {
        float result = A - B;
        while (result >= B) {
            result -= B;
        }
        return result;
    }
}
