package ru.ncedu.java.tasks;

import javafx.collections.transformation.SortedList;
import sun.reflect.generics.tree.Tree;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;

public class DateCollectionsImpl implements DateCollections {
    public DateCollectionsImpl() {
    }

    private DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
    private Map<String, Element> mainMap = null;

    private static Comparator comparator = new Comparator<Element>() {
        @Override
        public int compare(Element e1, Element e2) {
            if (e1.getBirthDate().getTime().getTime() >
                    e2.getBirthDate().getTime().getTime()) {
                return 1;
            } else if (e1.getBirthDate().getTime().getTime() <
                    e2.getBirthDate().getTime().getTime()) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    private static Comparator comparatorDeath = new Comparator<Element>() {
        @Override
        public int compare(Element e1, Element e2) {
            if (e1.getDeathDate().getTime().getTime() >
                    e2.getDeathDate().getTime().getTime()) {
                return 1;
            } else if (e1.getDeathDate().getTime().getTime() <
                    e2.getDeathDate().getTime().getTime()) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    @Override
    public void setDateStyle(int dateStyle) {
        dateFormat = DateFormat.getDateInstance(dateStyle);
    }

    @Override
    public Calendar toCalendar(String dateString) throws ParseException {
        return toCalendar(dateFormat.parse(dateString));
    }

    private Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    @Override
    public String toString(Calendar date) {
        return dateFormat.format(date.getTime());
    }

    @Override
    public void initMainMap(int elementsNumber, Calendar firstDate) {
        mainMap = new TreeMap<String, Element>();
        for (int i = 0; i < elementsNumber; i++) {
            int lifeTime = (int) (Math.random() * 2000);
            Calendar birthDate = (Calendar) firstDate.clone();
            birthDate.add(Calendar.DATE, i * 110);
            Element element = new Element(birthDate, lifeTime);
            String dataString = Long.toString(birthDate.getTime().getTime());
            mainMap.put(dataString, element);
        }
    }

    @Override
    public void setMainMap(Map<String, Element> map) {
        mainMap = map;
    }

    @Override
    public Map<String, Element> getMainMap() {
        return mainMap;
    }

    /**
     * Returns those entries of the main map, where elements' <code>birthDate</code> &gt; today
     *  (i.e. elements to be born in future: not today and not in the past).
     * The resulting sub-map must be sorted according to the <code>birthDate</code>
     * (ascending order of dates; but it's not alphabetic natural order of keys!).
     * @return sorted map containing a subset of the {@link #getMainMap()}
     */
    @Override
    public SortedMap<String, Element> getSortedSubMap() {
        SortedMap<String, Element> subMap = new TreeMap<String, Element>();
        Date today = Calendar.getInstance().getTime();
        for (Map.Entry<String, Element> entry : mainMap.entrySet()) {
            if (today.getDate() < entry.getValue().getBirthDate().getTime().getTime()) {
                subMap.put(entry.getKey(), entry.getValue());
            }
        }
        return subMap;
    }
    /**
     * Represents values of the main map as a {@link List}.
     * The list must be sorted according to the <code>birthDate</code> (ascending order).
     * @return list with all the values of the {@link #getMainMap()}.
     */
    @Override
    public List<Element> getMainList() {
        List<Element> list = new ArrayList<>();
        for (Map.Entry<String, Element> entry : mainMap.entrySet()) {
            list.add(entry.getValue());
        }
        Collections.sort(list, comparator);
        return list;
    }

    /**
     * Sorts the given list according to the <code>deathDate</code> (ascending order).<br/>
     * Don't use {@link List#sort(Comparator)} method because it's since Java 8
     * (and Java version in Skill Bench is 7 now). Use a method of {@link Collections}.
     */
    @Override
    public void sortList(List<Element> list) {
        Collections.sort(list, comparatorDeath);
    }
    /**
     * Removes some elements from the given list. The <code>birthDate</code> of removing elements
     *  must be in winter (i.e. in December, January or February).<br/>
     * Hint: remove elements via {@link Iterator}.
     */
    @Override
    public void removeFromList(List<Element> list) {
        for (Iterator<Element> i = list.iterator(); i.hasNext();) {
            switch (i.next().getBirthDate().getTime().getMonth()) {
                case Calendar.JANUARY  :
                case Calendar.FEBRUARY :
                case Calendar.DECEMBER :
                    i.remove();
            }
        }
    }
}
