package ru.ncedu.java.tasks;

public class EmployeeImpl implements Employee {
    public EmployeeImpl() {
    }

    private String firstName = null;
    private String lastName = null;
    private int salary = 1000;
    private Employee manager = null;

    @Override
    public int getSalary() {
        return salary;
    }

    @Override
    public void increaseSalary(int value) {
        salary += value;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getFullName() {
        return firstName + " " + lastName;
    }

    @Override
    public void setManager(Employee manager) {
        this.manager = manager;
    }

    @Override
    public String getManagerName() {
        return manager == null ? "No manager" : manager.getFullName();
    }

    @Override
    public Employee getTopManager() {
        if (manager == null) {
            return this;
        } else {
            return manager.getTopManager();
        }
    }

}
