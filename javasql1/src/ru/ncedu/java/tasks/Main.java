package ru.ncedu.java.tasks;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        DateCollections collections = new DateCollectionsImpl();
        collections.initMainMap(10, calendar);
        System.out.println(collections.getMainMap());
        System.out.println(collections.getSortedSubMap());
        System.out.println(collections.getMainList());
        List<DateCollections.Element> list = collections.getMainList();
        collections.sortList(list);
        System.out.println(list);
        collections.removeFromList(list);
        System.out.println(list);

    }
}
